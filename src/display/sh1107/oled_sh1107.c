/*!
 * \file      lcd_sh1107.c
 *
 * \brief     Target sh1107 board display implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *
 * \endcode
 *
 * \author    Anol P. ( EmOne ) <anol.p@emone.co.th>
 */

#include "oled_sh1107.h"

void init_oled_sh1107(void)
{
    /* Do nothing */
}


